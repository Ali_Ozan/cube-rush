﻿using UnityEngine;
using UnityEngine.UI;

public class LevelCompleteScore : MonoBehaviour
{
    public Text LevelCompleteScoreText;

    void Update()
    {
        if (GameManager.LevelHasCompleted == true)
        {
            LevelCompleteScoreText.text = ("Level Score: " + ScoreBoard.LevelScore.ToString("0"));
        }
    }
}
