﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class RestartFailedLevel : MonoBehaviour
{
    public void LoadFailedLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameManager.LevelHasCompleted = false;
    }
}
