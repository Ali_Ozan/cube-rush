﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuToMainGameSceneSkip : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 3);
    }
}
