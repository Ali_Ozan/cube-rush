﻿using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement movement; // A reference to our player movement script

    //This function runs when we hit another object
    //We get information about the collision and we call it "collisionInfo"
    void OnCollisionEnter (Collision CollisionInfo)
    {   
        if (CollisionInfo.collider.tag == "Obstacle") //Checking if the object we collide has a tag called "Obstacle"
        {
            movement.enabled = false;
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}
