﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;

    /*// Start is called before the first frame update
    void Start()
    {
        rb.AddForce(0, 200, 500);
    }*/
    public float forwardForce = 400f;
    public float sidewaysForce = 400f;

    // Update is called once per frame
    void FixedUpdate()
    {   
        //Add a forward force
        rb.AddForce(0, 0, forwardForce * 1.2f* Time.deltaTime);

        if (Input.GetKey("d") || Input.GetKey("right")) 
            rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        
            
        if (Input.GetKey("a") || Input.GetKey("left"))
            rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);

        if (Input.GetKey("w") || Input.GetKey("up"))
            rb.AddForce(0, 0, forwardForce * 1.2f *Time.deltaTime);

        if (Input.GetKey("s") || Input.GetKey("down"))
            rb.AddForce(0, 0, -0.4f * forwardForce * Time.deltaTime);

        if (rb.position.y < -2 && GameManager.LevelHasCompleted==false)
            FindObjectOfType<GameManager>().EndGame();
    }   
}
