﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreCounting : MonoBehaviour
{
    public Transform player;
    public Text ScoreText;
    public Color Yellow = new Color(1f, 1f, 0.4f, 1f);
    public Color Red = new Color(1f, 0.2f, 0.2f, 1f);
    public Color White = new Color(1f, 1f, 1f, 1f);

    public float ScorePoint = 0f;
    public float ScorePoint_temp = 0f;
    public float CurrentPositionZ = 0f;

    public float ThePointWpressed = 0f;
    public float BonusPointsDueToW = 0f;
    public float BonusPointsDueToW_temp = 0f;

    public float ThePointSpressed = 0f;
    public float LostPointsDueToS = 0f;
    public float LostPointsDueToS_temp = 0f;

    public float ThePointNoButtonStateStarted = 0f;
    public float PointsWithNoButtons = 0f;
    public float PointsWithNoButtons_temp = 0f;

    public bool wSwitch = false;
    public bool sSwitch = false;
    public bool NoButtonsSwitch = false;

    public bool TotalScoreIncreased = false;

    // Update is called once per frame
    void Update()
    {
        if (GameManager.LevelHasCompleted == false)
        {
            CurrentPositionZ = player.position.z;
            if (Input.GetKey("w") || Input.GetKey("up"))
            {
                ScoreText.color = Yellow;
                ScoreText.fontSize = 65;
                ScoreText.fontStyle = FontStyle.Normal;
                if (wSwitch == false)
                {
                    ThePointWpressed = CurrentPositionZ;
                    wSwitch = true;
                    ScorePoint_temp = ScorePoint;
                }
                if (sSwitch == true)
                {
                    sSwitch = false;
                    LostPointsDueToS += LostPointsDueToS_temp;
                }
                if (NoButtonsSwitch == true)
                {
                    NoButtonsSwitch = false;
                    PointsWithNoButtons += PointsWithNoButtons_temp;
                }
                BonusPointsDueToW_temp = (CurrentPositionZ - ThePointWpressed) * 2;
                ScorePoint = ScorePoint_temp + BonusPointsDueToW_temp;
            }
            else if (Input.GetKey("s") || Input.GetKey("down"))
            {
                ScoreText.color = Red;
                ScoreText.fontSize = 50;
                ScoreText.fontStyle = FontStyle.Normal;
                if (sSwitch == false)
                {
                    ThePointSpressed = CurrentPositionZ;
                    sSwitch = true;
                }
                if (wSwitch == true)
                {
                    wSwitch = false;
                    BonusPointsDueToW += BonusPointsDueToW_temp;
                }
                if (NoButtonsSwitch == true)
                {
                    NoButtonsSwitch = false;
                    PointsWithNoButtons += PointsWithNoButtons_temp;
                }
                LostPointsDueToS_temp = CurrentPositionZ - ThePointSpressed;
            }
            else
            {
                ScoreText.color = White;
                ScoreText.fontSize = 50;
                ScoreText.fontStyle = FontStyle.Normal;
                if (NoButtonsSwitch == false)
                {
                    ThePointNoButtonStateStarted = CurrentPositionZ;
                    NoButtonsSwitch = true;
                    ScorePoint_temp = ScorePoint;
                }
                if (sSwitch == true)
                {
                    sSwitch = false;
                    LostPointsDueToS += LostPointsDueToS_temp;
                }
                if (wSwitch == true)
                {
                    wSwitch = false;
                    BonusPointsDueToW += BonusPointsDueToW_temp;
                }
                PointsWithNoButtons_temp = CurrentPositionZ - ThePointNoButtonStateStarted;
                ScorePoint = ScorePoint_temp + PointsWithNoButtons_temp;
            }
            TotalScoreIncreased = false;
        }
        else
        {
            ScoreBoard.LevelScore = ScorePoint;
            if (!TotalScoreIncreased && SceneManager.GetActiveScene().buildIndex > 1 && ScorePoint >= 1000)
            {
                TotalScoreIncreased = true;
                ScoreBoard.OverallScore += ScorePoint;
            }
        }
            
        if (FindObjectOfType<GameManager>().gameHasEnded == false)
            ScoreText.text = ScorePoint.ToString("0");
        else
        {
            ScoreText.text = "GAME OVER";
            ScoreText.color = White;
            ScoreText.fontSize = 55;
            ScoreText.fontStyle = FontStyle.Normal;
        }
    }
}

