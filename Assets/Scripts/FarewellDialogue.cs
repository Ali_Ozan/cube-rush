using UnityEngine;
using UnityEngine.UI;

public class FarewellDialogue : MonoBehaviour
{
    public Transform player;
    public Text FarewellText;
    public float CurrentPositionZ = 0f;
    public float CurrentPositionY = 0f;
    void Update()
    {
        CurrentPositionZ = player.position.z;
        CurrentPositionY = player.position.y;
        if (CurrentPositionZ > 400f && CurrentPositionZ < 500f)
            FarewellText.text = "The game is almost finished!";
        else if (CurrentPositionZ > 500f && CurrentPositionZ < 600f)
            FarewellText.text = "What a challenge eh?";
        else if (CurrentPositionZ > 600f && CurrentPositionZ < 700f)
            FarewellText.text = "Remember one thing...";
        else if (CurrentPositionZ > 700f && CurrentPositionZ < 800f)
            FarewellText.text = "When something seems impossible...";
        else if (CurrentPositionZ > 800f && CurrentPositionZ < 930f)
            FarewellText.text = "Try to think outside the box...";
        else if (CurrentPositionZ > 950f && CurrentPositionZ < 1000f && CurrentPositionY > 0.9)
            FarewellText.text = "VOILA!, YOU'VE DONE IT!";
        else
            FarewellText.text = "";
    }
}
