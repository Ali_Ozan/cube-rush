﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public bool gameHasEnded = false;
    static public bool LevelHasCompleted = false;
    static public bool LevelHasWon = false;
    public float restartDelay = 1f;
    public GameObject completeLevelUI;
    public GameObject FailLevelUI;
    public void CompleteLevel()
    {
        LevelHasCompleted = true;
        if (FindObjectOfType<ScoreCounting>().ScorePoint > 1000) //I'm not allowing the level to be won if the player slows frequently and not compensating it by speeding up and gaining at least 1000p in 1000meters.
        {
            LevelHasWon = true;
            completeLevelUI.SetActive(true);
        }
        else
            FailLevelUI.SetActive(true);
    }

    public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("Game Over");
            FindObjectOfType<ScoreCounting>().ScoreText.text = "GAME OVER";
            FindObjectOfType<ScoreCounting>().ScoreText.fontSize = 50;
            Invoke("Restart", restartDelay);
        }
    }

    public void Restart ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}