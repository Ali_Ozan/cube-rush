﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    public Text LevelScoreText;
    public Text TotalScoreText;
    static public float OverallScore = 0f;
    static public float LevelScore = 0f;
    void Update()
    {
        LevelScoreText.text = "Level Score: " + LevelScore.ToString("0");
        TotalScoreText.text = "Total Score: " + OverallScore.ToString("0");
    }
}
