﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Replay : MonoBehaviour
{
    public void ReplayLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex > 2)//in order to avoid subtraction of the points if the player replays the tutorial
            ScoreBoard.OverallScore -= ScoreBoard.LevelScore;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

}
