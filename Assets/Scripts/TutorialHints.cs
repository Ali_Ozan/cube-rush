﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialHints : MonoBehaviour
{
    public Transform player;
    public Text HintText;
    public float CurrentPositionZ = 0f;
    void Update()
    {
        CurrentPositionZ = player.position.z;
        if (CurrentPositionZ < 100f)
            HintText.text = "Welcome to Cube Rush!";
        else if (CurrentPositionZ > 100f && CurrentPositionZ < 210f)
            HintText.text = "The game is fairly simple my friend";
        else if (CurrentPositionZ > 210f && CurrentPositionZ < 320f)
            HintText.text = "Just flow through the way and reach the endpoint";
        else if (CurrentPositionZ > 320f && CurrentPositionZ < 430f)
            HintText.text = "Do not hit the obstacles and do not fall off the edges";
        else if (CurrentPositionZ > 430f && CurrentPositionZ < 540f)
            HintText.text = "Use W  A  S  D keys (or arrow keys) to manipulate the cube's movement";
        else if (CurrentPositionZ > 540f && CurrentPositionZ < 660f)
            HintText.text = "A and D buttons (or right&left arrow) lets you move sideways";
        else if (CurrentPositionZ > 660f && CurrentPositionZ < 800f)
            HintText.text = "S (or Down) key slows you down, but you can't gain points while you push it";
        else if (CurrentPositionZ > 800f && CurrentPositionZ < 950f)
            HintText.text = "W (or Up) speeds you up and you gain 2x points while speeding!";
        else if (CurrentPositionZ > 950f && CurrentPositionZ < 1100f)
            HintText.text = "That's all you need to know, enjoy the ride!";
        else
            HintText.text = "";
    }
}
